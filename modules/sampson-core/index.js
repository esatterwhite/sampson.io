'use strict';
/**
 * index.js
 * @module index.js
 * @author 
 * @since 0.0.1
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 */

var fn = require('./fn')

exports.createCallback = fn.createCallback
exports.noop = fn.noop;
exports.paths = require('./paths')
