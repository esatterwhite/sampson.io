'use strict';
/**
 * fn.js
 * @module fn.js
 * @author 
 * @since 0.0.1
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 */

var noop = new Function();

exports.createCallback = function( args ){
	return typeof args[ args.length -1 ] == "function" ? args[ args.length-1 ] : noop
};

exports.noop = noop;
