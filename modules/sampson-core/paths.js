/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'use strict';
/**
 * module containing common location paths 
 * compaitible class.
 * @module sampson-conf/options
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires path
 */

var path = require( 'path' );

exports.root    = path.normalize( path.resolve(__dirname, '..', '..') );
exports.lib     = path.resolve( exports.root, 'lib' );
exports.modules = path.resolve( exports.root, 'modules' );
