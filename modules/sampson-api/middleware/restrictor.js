
'use strict';
/**
 * restrictor.js
 * @module restrictor.js
 * @author 
 * @since 0.0.1
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 */
var conf = require( 'sampson-conf' )
exports = module.exports = function( ){

	return function restrictor( request, response, next ){
		var ua = request.headers["user-agent"] || ""
		ua = ua.toLowerCase();
		//console.log( ua )
		var isBrowser = ua.match(/(opera|ie|firefox|chrome|webos|android|blackberry)|(ip(?:ad|od|hone))/)
		var restricted= {
			post:true
			,head:true
			,options:true
			,put:true
			,"delete":true
		};

		var method = request.method.toLowerCase();

		if( !!isBrowser && method in restricted ){
			response.statusCode = 403
			response.end("Forbidden");
			return;
		}
		else{
			return next()
		}
	}
}
