'use strict';
/**
 * push.js
 * @module push.js
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires express
 */

var express = require( 'express' )
  , util    = require( 'util' )
  , router
  ;

const UUID_REGEX = '[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}'

module.exports = router = new express.Router();

router.post('/', function( req, res, next){
	console.log( req.headers )
	return res.status(200).send( "data" )
})

router.post( util.format('/:session(%s)', UUID_REGEX), function( req, res, next ){
	return res.status(201).send(util.format('push to session %s', req.params.session) )
})

router.post('/:room', function( req, res, next ){
	return res.status(202).send(util.format('push to %s', req.params.room ) )
})

router.post('/private-:room', function( req, res){

})
