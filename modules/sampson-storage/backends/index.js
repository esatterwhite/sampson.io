'use strict';
/**
 * Data storage hook for sampson. Bootstraps a storage engine and allows for 
 * some dynamic configuration and setup.
 * @module index.js
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires prime
 * @requires assert
 * @requires sampson-log
 * @requires sampson-conf
 * @requires module:sampson-storage/backends/memory
 * @requires module:sampson-storage/backends/file
 * @requires module:sampson-storage/backends/redis
 * @requires module:sampson-storage/backends/mongo
 * @requires module:sampson-storage/backends/rethink
 */

var prime   = require( 'prime' )
  , assert  = require( 'assert' )
  , util    = require( 'util' )
  , logger  = require( 'sampson-log' )
  , conf    = require( 'sampson-conf' )
  , Memory  = require( './memory' )
  , File    = require( './file' )
  , Redis   = require( './redis' )
  , Mongo   = require( './mongo' )
  , Rethink = require( './rethink' )
  , current_backend
  , backendType
  , backendConfig
  , engines
  ;

engines = {
	'redis':Redis
  , 'file': File
  , 'memory': Memory
  , 'mongo' : Mongo
  , 'rethink': Rethink
  , 'default': Memory
};

function isStorageLike( obj ){
	var instance;
	if( typeof obj == "function"){
		instance = new obj();
	} else {
		instance = obj;
	}
	try{
		assert.equal( ( typeof instance.get ), "function" );
		assert.equal( ( typeof instance.set), "function" );
		assert.equal( ( typeof instance.flush), "function" );
		assert.equal( ( typeof instance.close), "function" );
	} catch( e ){
		logger.error('unable to duck punch a storage object!');
		return false;
	}
	return true;
};

exports.register = function(){
	if( isStorageLike( backend )  ){
		if( engines.hasOwnProperty( name ) ){
			logger.error( "A Storage engine with the name %s already exists",name );
		} else{
			engines[name] =  backend
			return backend;
		}
	} else {
		logger.warning( "Attempted to register a non storage like object - %s", name );
		return null;
	}
};

/**
 * Sets the internale storage backend to the specified setting with passed config options. If no options are specified sampson will look in the current application configuration for a storage setting of the matching name ( storage:`name` ).
 * @param {String} name The name of the storage backend to use ( memory, redis, file, mongo, rethink )
 * @param {Object} [config={}] Configuration options specific to the backend in question.
 * @return {Backend} The current backend instance
 * */
exports.use = function( name, config ){
	var backend = engines[ name ];
	var backendConf = config || conf.get('storage:'+ name ) || {};
	if( !backend ){
		var e = new Error();
		e.name = 'ImproperlyConfigured';
		e.message = name + ' is not a valid storage backend';

		throw e;
	}
	
	current_backend = typeof backend === 'function' ? new backend( backendConf ) : backend;
	return current_backend;
};

Object.defineProperties( exports,{
	current: {
		writeable: false,
		configurable: false,
		enumerable: false,
		get: function(){
			if( !current_backend ){
				let storage_type = conf.get('with-storage') || 'default' ;
				let backend = engines[ storage_type ];
				let backendconfig = conf.get('storage:' + storage_type) || {name:storage_type};
				current_backend = new engines[ storage_type ]( backendconfig );
			}
			return current_backend;
		}
	}
});
exports.Redis = Redis;
exports.File = File;
exports.Memory = Memory;
exports.Rethink = Rethink;
exports.Mongo = Mongo;
