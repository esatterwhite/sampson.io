/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'strict mode'
/**
 * Base in memory storage for sampson
 * @module module:sampson-storage/backends/memory
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires prime
 * @requires module:lib/class/options
 * @requires options
 * @requires events
 * @requires sampson-core
 * @requires module:mout/object/get
 * @requires module:mout/object/set
 * @requires module:mout/object/values
 * @requires module:mout/array/remove
 */

var Prime    = require( 'prime' )                      // Prime
  , Options  = require( '../../../lib/class/options' ) // Options
  , events   = require( 'events' )                     // node events module
  , core     = require( 'sampson-core' )               // core
  , conf     = require( 'sampson-conf' )               // conf
  , log     = require( 'sampson-log' )               // conf
  , _get     = require('mout/object/get')              // get
  , _set     = require('mout/object/set')              // set
  , values   = require('mout/object/values')           // values
  , remove   = require('mout/array/remove')            // remove
  , combine  = require( 'mout/array/combine')
  , compact  = require( 'mout/array/compact')
  , unique   = require( 'mout/array/unique')
  , typecast = require('mout/string/typecast')            // typecast
  , isObject = require('mout/lang/isObject')            // isObject
  , redis    = require( 'redis' )
  , RedisStore;
  ;

const separator = '.'
// TODO: expires in redis are cleared if the value is 
// changed using SET. TTL should be set at the store level or 
// as a parameter to the methods to enable operations
// in a single call
module.exports = RedisStore = Prime({

	inherits:events.EventEmitter
	,mixin:Options
	,options:{
		name:null
		,backend:{
			name:'redis'
			,db:4
			,host:'localhost'
			,port:6379
			,opts:{ }
		}
	}
	,constructor: function( options ){
		this.setOptions( options );
		var backend = this.options.backend;
		this.cli = redis.createClient( backend.port, backend.host, backend.opts );
		this.cli.select( backend.db || 1)
		this.cache = {};
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,get: function get( store, keypath ){
		var key
			,that = this
			,callback
			,hashkey
			,evt;

		callback = arguments[ arguments.length-1 ];
		if( typeof keypath === "string"){
			keypath = keypath.split( separator )
		}
		key = [store,keypath.shift()].join( separator )
		hashkey = keypath.join(separator )

		this.cli.get( key, function( err, val ){

			if( err ){
				callback( err, null)
				that.emit('error', err);
			} else{
				try {
					val = JSON.parse( val );
					val = !!hashkey ? _get( val, hashkey) : val
				} catch( e ){
					val = typecast( val )
				}
				if( typeof callback === "function" ){
					callback( err, val )
				}
			}
		});
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, set: function set( store, keypath, value ){
		var that = this
		  , callback = arguments[ arguments.length - 1 ];
		this.get( store, keypath, function( err, data ){
			var key
			data = data || {}
			if( typeof value != "string" ){
				value = JSON.stringify( value )
			}
			if( typeof keypath == "string" ){
				keypath = keypath.split( separator );
			}


			key = keypath.shift( );

			key = [store, key].join( separator );
			_set( data, keypath.join( separator ), value )
			that.cli.set( key, JSON.stringify( data ), function(err){
				if( err ){
					callback( err, null)
					that.emit( "error", err);
					return;
				} else{
					if( typeof callback === "function" ){
						callback( null, value );
					}
				}
			});
		})

		return this;
	}

	/**
	 * Removes an item from a list at a specified value
	 * @method slice
	 * @param {String} namespace ...
	 * @param {String} key ...
	 * @param {Mixed} value ...
	 * @param {Function} [callback] ...
	 * @return
	 **/
	, slice: function slice( store, keypath, value ){
		  var key
			,hashname
			,hashkey
			,callback;

		if( typeof keypath == "string" ){
			keypath = keypath.split( separator )
		}
		hashname = keypath.shift(); // "sdfasdf4-asdf24-asdf-2-asdf24"

		key = [store,hashname].join( separator ) // sessions.sdfasdf4-asdf24-asdf-2-asdf24
		callback = arguments[ arguments.length - 1 ];

		hashkey = keypath.join( separator ) // "applications"

		this.cli.get( key, function(err, reply){
			var evt = "drain"

				,current_set

			reply = JSON.parse( reply ) || {};
			current_set = _get( reply, hashkey)
			current_set = Array.isArray( current_set ) ? current_set : compact( [current] )

			var idx = current_set.indexOf(  value );

			if( idx !== -1){
				current_set.splice( idx,1 )
				_set( reply, hashkey, current_set );
			}

			this.cli.set( key, JSON.stringify( reply ), function( err, reply ){
				if( err ){
					callback && callback( err, null )
					this.emit( "error", err);
					return;
				} else{
					if( typeof callback === "function" ){
						callback( null, current_set );
					}
				}
			}.bind( this ))

		}.bind( this ));
		return this;
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, push: function push( store, attrpath, value, all ){
		// ["sessions", "sdfasdf4-asdf24-asdf-2-asdf24.applications", "blacjack"]
		if( typeof attrpath == "string" ){
			attrpath = attrpath.split( separator )
		}
		// ["sessions", ["sdfasdf4-asdf24-asdf-2-asdf24", "applications"], "blacjack"]

		// logger.info(attrpath)
		if( attrpath.length > 2){
			throw "Maximum Store depth exceeded"
		}

		var key
			,hashname
			,hashkey
			,callback;

		hashname = attrpath.shift(); // "sdfasdf4-asdf24-asdf-2-asdf24"
		key = [store,hashname].join( separator ) // sessions.sdfasdf4-asdf24-asdf-2-asdf24
		callback = arguments[ arguments.length - 1 ];

		hashkey = attrpath.join(separator ) // "applications"

		this.cli.get( key, function(err, reply){
			var evt = "drain"
				,current_set

			reply = JSON.parse( reply ) || {};
			current_set = _get( reply, hashkey )
			current_set =  Array.isArray( current_set ) ? current_set : [current_set]

			value = Array.isArray( value ) ? value : [value]
			current_set = unique( compact( current_set.concat( value ) ) );

			_set( reply, hashkey, current_set );

			this.cli.set( key, JSON.stringify( reply ), function( err, reply ){

				if( err ){
					callback( err, null)
					this.emit( "error", err);
					return;
				} else{
					if( typeof callback === "function" ){
						callback( null, value );
					}
				}
			}.bind( this ))

		}.bind( this ));
		return this;
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, ttl: function ttl( store, key, sec ){
		var callback;


		callback = arguments[ arguments.length -1 ];
		key = store + separator + key


		this.cli.expire(key, sec, function( err, replies ){
			if( err ){
				// logger.error( err.message )
				this.emit('error', err )
			} else{
				callback(err, replies );
			}
		})

	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
   , close: function close( cb ){
		this.cli.quit( cb );
   }

   /**
	* DESCRIPTION
	* @method NAME
	* @param {TYPE} NAME
	* @param {TYPE} NAME
	* @return
	**/
   , remove: function remove( store, key, cb ){}

   /**
	* DESCRIPTION
	* @method NAME
	* @param {TYPE} NAME
	* @param {TYPE} NAME
	* @return
	**/
   , all: function all( store ){}

   /**
	* DESCRIPTION
	* @method NAME
	* @param {TYPE} NAME
	* @param {TYPE} NAME
	* @return
	**/
  , flush: function flush( store, cb ){
		var data = {}
			,that = this
			;

	callback = arguments[ arguments.length-1 ];

	this.cli.keys( [store , "*"].join( separator ) , function( err, keys ){
		if( !keys.length ){
			return cb && cb( null, 0 );
		}
		that.cli.del( keys, function( e1, reply ){

			if( typeof cb === "function" ){
				try{
					cb(e1, reply );
					if( e1 ){
						that.emit('error', e1 )
					}
				} catch( e2 ){
					// logger.debug("problem executing backend flush callback");
					that.emit('error', e2)
				}
			}
			that.emit( "flush" );
		});
	});
	return this;
   }
});
