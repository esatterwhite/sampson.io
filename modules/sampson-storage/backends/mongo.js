'use strict';
/*jshint laxcomma:true, smarttabs: true */
/**
 * A Mongo DB backend for Middleman storage
 * @module module:sampson-storage/backends/mongo
 * @since 0.0.1
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires path
 * @requires fs
 * @requires util
 * @requires class
 * @requires core
 * @requires string
 * @requires array
 * @requires mongoskin
 * @requires sampson-storage.Base
 **/
var  path         = require( 'path' )                // node path module
	, util        = require( 'util' )                // node util module
	, events        = require( 'events' )                // node events module
	, core        = require( 'sampson-core' )        // core
	, conf        = require( 'sampson-conf' )        // conf
	, log         = require( 'sampson-log' )         // conf
	, mongo       = require('mongoskin' )            // mongodb driver module
	, _get        = require('mout/object/get' )      // get
	, _set        = require('mout/object/set' )      // set
	, values      = require('mout/object/values' )   // values
	, remove      = require('mout/array/remove' )    // remove
	, combine     = require( 'mout/array/combine' )
	, compact     = require( 'mout/array/compact' )
	, unique      = require( 'mout/array/unique' )
	, typecast    = require('mout/string/typecast' ) // typecast
	, interpolate = require('mout/string/interpolate' ) // typecast
	, isObject    = require('mout/lang/isObject' )   // isObject
	, Class       = require('../../../lib/class' )
	, Options     = require('../../../lib/class/options' )
	, basePath                                    // path to braveheart lib
	, MongoStore                                  // Mongo Middleman instance
	, createCallback
	;

const SEPARATOR = '.';
createCallback = core.createCallback;
/**
 * MongoDB Backend for Middleman
 * @class module:sampson-storage/backends/mongo.MongoStore
 * @param {Object} options The configuration options for the class
 * @example var x = new NAME.Thing({});
 */
module.exports = MongoStore = new Class(/** @lends module:sampson-storage/backends/mongo.MongoStore.prototype */{
	mixin:[ Options, events.EventEmitter ]
	,options:{
		 host:"localhost"
		,port:27017
		,db:"sampson-storage"
	}

	/**
	 * This is the constructor
	 * @constructor
	 * @param {Object} options Initial class configuration
	 * @param {String} [options.host=localhost] The url / ip / host name of the mongodb server to connect to
	 * @param {number} [options.port=27017] The port of the Mongodb server to connect to
	 * @param {string} [options.db=middleman] The name of the mongo db to create and store data in
	 */
	,constructor: function( options ){
		this.setOptions( options );
		var str = interpolate( 'mongodb://{{host}}:{{port}}/{{db}}', this.options ) ;
		this.cli = mongo.db( str, { safe: false } );
		this.ensureIndex(this.options.name, this.options.index )
	}

	,ensureIndex: function(store, index) {
		index = Array.isArray( index ) ? index : [index]

		var desiredIndicies = {};
		for (var i = 0; i < index.length; i++) {
			desiredIndicies[index[i]] = 1;
		}
		this.cli.collection(store).ensureIndex( desiredIndicies, function( err, values){
			this.emit('connect', err, values )
		}.bind( this ));
	}

	/**
	 * returns all records in the storage collection
	 * @method module:sampson-storage/backends/mongo.MongoStore#all
	 * @param {String} store The name of db collection to read from
	 * @return
	 **/
	,all: function( store ){
		var callback;

		callback = core.createCallback( arguments )
		this.cli
			.collection( store )
			.find({ "_id": { $exists:true } })
			.toArray( callback );
	}

	/**
	 * Appends a value to an array
	 * @method module:sampson-storage/backends/mongo.MongoStore#push
	 * @param {String} store The name of the collection to search through
	 * @param {String} keypath search path to a field
	 **/
	, push: function( store, keypath, value ){
		var  callback
			,sid
			,field
			,key
			,element;

		element 	    = {}
		callback		= createCallback( arguments );

		if( typeof keypath === "string" ){
			keypath = keypath.split( SEPARATOR );
		}

		key			= keypath.shift( )
		key			= [store, key].join( SEPARATOR );
		keypath   = keypath.join( SEPARATOR );

		value = Array.isArray( value )  ? value : [value]
		element[ keypath ] = {$each: value}
		this.cli
			.collection( store )
			.update(
				 { pk: key }
				,{ $addToSet: element }
				,{ upsert: true }
				,callback
			);
	}

	/**
	 * Updates a document in the store
	 * @method module:sampson-storage/backends/mongo.MongoStore#update
	 * @param {String} store The name of the collection to search through
	 * @param {String} keypath search path to a field
	 **/
	, update: function( store, keypath, value ){
		var  callback
			,key
			,SEPARATOR
			,that = this;


		if( typeof keypath === "string" ){
			keypath = keypath.split( SEPARATOR );
		}

		callback	= createCallback( arguments );
		key			= keypath.shift( )
		key			= [store, key].join( SEPARATOR )

		value.pk = key;

		this.cli
			.collection( store )
			.update( { pk: value.pk }, value, { safe: true, upsert: true }, function( err, records ){
			if ( err ){
				if( typeof callback === 'function' ){
					callback( err, null );
					that.fireEvent( 'error', err );
				}
			} else {
				if( typeof callback === 'function' ){
					var emitter = new Events();
					callback( err, emitter );
					setTimeout( function(){
						emitter.fireEvent( 'drain', [ records ] )
					}, 1 )
				}
			}
		});
	}

	/**
	 * Closes the connection to MongoDB
	 * @chainable
	 * @method module:sampson-storage/backends/mongo.MongoStore#close
	 * @return {MongoStore} The current class instance
	 **/
	, close: function(){
		this.cli.close();
		return this;
	}

	/**
	 * purgess all data in a collection
	 * @method module:sampson-storage/backends/mongo.MongoStore#flush
	 * @param {String} store the name of the db collection to drop
	 **/
	, flush: function( store ){
		var callback;

		callback = createCallback( arguments );
		this.cli.collection( store ).remove( callback )
	}

	/**
	 * Reads a value from the data store. if no value path is specified, the entire data record is returned
	 * @method module:sampson-storage/backends/mongo.MongoStore#get
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, get: function( store, keypath ){
		var callback
			,key
			;

		callback = createCallback( arguments )

		if( typeof keypath === "string"){
			keypath = keypath.split( SEPARATOR );
		}

		// document key lookup
		key			= keypath.shift( )
		key			= [store, key].join( SEPARATOR );

		this.cli
			.collection( store )
			.findOne(
				{ pk: key }
				,function( err, record ){
					var value;
					if(!err) {

						// read the value out of the document
						if( keypath.length && record){
							value = _get( record, keypath.join( SEPARATOR ) );
						} else{
							value = record;
						}
					}
					callback( err, value );
			});
	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/mongo.MongoStore#remove
	 * @param {String} NAME ...
	 * @param {String} NAME ...
	 * @return
	 **/
	, remove: function( store, keypath ){
		var callback;

		callback = createCallback( arguments );

		this.cli
			.collection( store )
			.remove(
				{ id: keypath }
				,callback
			);

	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/mongo.MongoStore#removeAll
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, removeAll: function( store, keypath ){
		var callback;

		callback = core.createCallback( arguments )

		this.cli.collection( store ).remove();

		if( typeof callback === 'function'){
			emitter = new Events();
			callback( null,emitter);

			emitter.fireEvent( "drain" );
			emitter.removeEvents();
		}
	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/mongo.MongoStore#ttl
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, ttl: function ttl( store, keypath, sec, callback ){
		var  callback
			,SEPARATOR
			,that = this;

		callback	= core.createCallback( arguments )
		callback	= typeof callback === "function" ? callback : function(){};

		// Set TTL on collection
		this.cli
			.collection( store )
			.ensureIndex(
				{ ttl: 1 }
				,{ expireAfterSeconds: sec }
				,function( ){
					var key;

					if( typeof keypath == "string"){
						keypath = keypath.split( SEPARATOR );
					}

					key			= keypath.shift( )
					key			= [store, key].join( SEPARATOR );

					that.cli
						.collection( store )
						.update(
						 	{ pk: key }
							,{ $set: { ttl: new Date() } }
							,{safe:true}
							, callback
						);
				}
			);

	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/mongo.MongoStore#pop
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, slice: function slice( store, keypath, value ){
		var  callback
			,sid
			,field
			,key
			,element = {}
			,arr = [];


		if( typeof keypath === "string" ){
			keypath = keypath.split( SEPARATOR );
		}

		callback	= createCallback( arguments );
		key			= keypath.shift()
		field		= keypath.shift()
		key			= [store, key].join( SEPARATOR )

		var collection = this.cli
			.collection( store );

		var q = {}
		q[field] = -1
		collection.update(
				 { pk: key }
				,{$pop: q}
				,callback
			);
	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/mongo.MongoStore#set
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, set: function set( store, keypath, value ){
		var  callback
			,key
			,that = this;


		if( typeof keypath === "string" ){
			keypath = keypath.split( SEPARATOR );
		}

		callback	= createCallback( arguments );
		key			= keypath.shift( )
		key			= [store, key].join( SEPARATOR )

		if( keypath.length ){
			var newval = {};
			newval.pk = key;
			if( keypath.length > 1 ){
				_set( newval, keypath.join( SEPARATOR ), value )
				value = newval;
			} else {
				newval[keypath] = value;
				value = newval
			}
		}

		this.cli
			.collection( store )
			.insert( value, { safe: true }, function( err, records ){
				callback( err, records )
			});
	}
	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/mongo.MongoStore#health
	 * @return
	 **/
	, health: function( ){
		var that = this;
		var state = 'NULL'
		if ( that.cli && that.cli.db) {
			state = that.cli.db._state;
		}
		return {
			name: 'mongo'
			,options: that.options
			,state: state
		}
	}
});

module.exports = MongoStore;
