'use strict';
/**
 * Base in memory storage for sampson
 * @module module:sampson-storage/backends/memory
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires prime
 * @requires module:lib/class/options
 * @requires options
 * @requires events
 * @requires sampson-core
 * @requires module:mout/object/get
 * @requires module:mout/object/set
 * @requires module:mout/object/values
 * @requires module:mout/array/remove
 */

var Prime    = require( 'prime' )                      // Prime
  , Options  = require( '../../../lib/class/options' ) // Options
  , events   = require( 'events' )                     // node events module
  , core     = require( 'sampson-core' )               // core
  , _get     = require('mout/object/get')              // get
  , _set     = require('mout/object/set')              // set
  , values   = require('mout/object/values')           // values
  , remove   = require('mout/array/remove')            // remove
  ;

/**
 * Description
 * @class module:memory.Store
 * @extends EventEmitter
 * mixes module:lib/class/options
 * @param {Object} options
 * @param {String} [options.name=null]
 * @param {Object} [options.backend]
 * @param {String} [options.backend.name="memory"]
 * @example var x = new memory.THING();
 */
module.exports = Prime(/* @lends module.THING.prototype */{
	inherits:events.EventEmitter
	,mixin:Options
	,options:{
		name:null
		,backend:{
			name:'memory'
			,opts:{ }
		}
	}
	,constructor: function( options ){

		this.setOptions( options )
		this.cli = {};
		this.cache = {};
	}

	/**
	 * Pushes a value on to an array. If the selected value is not an array, it will be converted to one
	 * @method module:sampson-storage/backends/memory.Store#push
	 * @param {String} key The to look up in the store. Can be a dot (.) separated string to traverse nested objects
	 * @param {Object} value the value to set at the specified key. Can be any valid JSON serializable object
	 * @param {Function} [callback] a function to be called when the operation is finished
	 **/
	, push: function push( store, key, value, cb ){
		var item = _get( this.cache, key ) || [];
		item  = Array.isArray( item ) ? item : [ item ];
		value = Array.isArray( value ) ? value : [ value ];
		item  = item.concat( value );
		_set( this.cache, key, item );
		this.emit( 'push', key, value, item );
		cb( null, item );
		return this;
	}

	/**
	 * Removed an item from an array. If the value at the specified key is not an array, it will be converted into one.
	 * @method module:sampson-storage/backends/memory.Store#slice
	 * @param {String} key The to look up in the store. Can be a dot (.) separated string to traverse nested objects
	 * @param {Object} value the value to set at the specified key. Can be any valid JSON serializable object
	 * @param {Function} [callback] a function to be called when the operation is finished
	 **/
	, slice: function slice( store, key, value, cb ){
				var item = _get( this.cache, key ) || [];
		item = Array.isArray( item ) ? item : [ item ];

		remove( item, value );
		_set( this.cache, key, item );
		this.emit( 'slice', key, value, item );
		cb(null, item );
		return this;
	}


	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/memory.Store#get
	 * @param {String} key The to look up in the store. Can be a dot (.) separated string to traverse nested objects
	 * @param {Object} value the value to set at the specified key. Can be any valid JSON serializable object
	 * @param {Function} [callback] a function to be called when the operation is finished
	 **/
	, get: function get( store, key, cb  ){
		cb(null, _get( this.cache, key ) );
		return this;
	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/memory.Store#set
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, set: function set( store, key, value ){
		var cb = core.createCallback( arguments )
		_set( this.cache, key, value);
		setImmediate( cb, null, this.cache );
		return this;
	}

	/**
	 * Forces a value to be undefined
	 * @method module:sampson-storage/backends/memory.Store#remove
	 * @param {String} key
	 * @param {Function} [callback]
	 * @return
	 **/
	, remove: function remove( store, key, cb ){
		return this.set( key, null, cb );
	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/memory.Store#flush
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, flush: function flush( store ){
		this.cache = {};
		setImmediate( 
			core.createCallback( arguments )
			, null
			, this.cache 
		);
		return this;
	}

	/**
	 * Sets a time to live on a key. After the specified time ( in seconds ) has
	 * has elapsed, the key will be set to null
	 * @method module:sampson-storage/backends/memory.Store#ttl
	 * @param {String} key
	 * @param {Number} seconds
	 * @param {Function} [callback]
	 **/
	, ttl: function ttl( store, key, seconds, cb ){
		cb = cb || core.createCallback( arguments );

		var id = setTimeout( function(){
			_set( this.cache, key, null );
			this.emit('key-expired', key );
		}.bind( this ), (seconds * 1000 ));

		setImmediate(cb, null, id);
		return this;;
	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/memory.Store#all
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, all: function all( store ){
		core.createCallback(arguments)(null, values( this.cache ));
		return this;
	}

	/**
	 * DESCRIPTION
	 * @method module:sampson-storage/backends/memory.Store#close
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, close: function close(){
		return this;
	}
});
