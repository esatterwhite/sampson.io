var assert = require('assert');
var Memory = require("../backends/memory");
describe('Memory Store', function(){
	var mem = new Memory({name:"memory"});
	

	describe("#get / #set", function(){
		before( function( done ){
			mem.flush( done );
		});

		it('should set nested values', function(done){
			 mem.set( null, 'foo.bar.baz', 1,function(){
			 	mem.get(null,'foo', function( err, data ){
			 		assert.equal( data.bar.baz, 1 );
			 		done();
			 	});
			 });
		});


		it('should expire keys', function( done ){
			mem.set(null,'key', 1, function(err){
				mem.get(null, 'key', function(err, data ){
					assert.equal( data, 1);
				});
				mem.ttl(null, 'key', 1, function(err){
					setTimeout(function(){
						mem.get(null,'key', function( err, key){
							assert.equal( key, null);
							done();
						});
					},1500);
				});
			});
		});


		it('should expire nested keys', function( done ){
			mem.set(null, 'key.baz',1);

			mem.set(null, 'key.bar', 2, function(err){
				mem.get(null, 'key.bar', function(err, data ){
					assert.equal( data, 2 );
				});
				mem.ttl(null, 'key.bar', 0.5, function(err){
					setTimeout(function(){
						mem.get(null, 'key', function( err, key){
							assert.equal( key.bar, null);
							assert.equal( key.baz, 1);
							done();
						});
					},600);
				});
			});
		});
	});

	describe('#push', function(){
		before( function( done ){
			mem.flush(null, done );
		});
		it('should convert values to arrays', function( done ){
			mem.set(null, 'test', 1, function( err1 ){
				mem.push(null, 'test', 2, function( err2){
					mem.get(null, 'test', function( err3, data ){
						assert.ok( Array.isArray( data ));
						assert.equal( data[0], 1 );
						assert.equal( data[1], 2 );
						done();
					});
				});
			});
		});


		it('should accept arrays of values', function( done ){
			mem.set(null, 'test', 1, function( err1 ){
				mem.push(null, 'test', [2,3,4], function( err2){
					mem.get(null, 'test', function( err3, data ){
						assert.ok( Array.isArray( data ));
						assert.equal( data[0], 1 );
						assert.equal( data[1], 2 );
						assert.equal( data[2], 3 );
						assert.equal( data[3], 4 );
						done();
					});
				});
			});
		});		
	});

	describe('#slice', function(){
		before(function( done ){
			mem.flush();
			mem.push(null,'values',[1,2,3,4], done)
		})


		it('should remove items from the array', function(done){
			mem.slice(null, 'values', 2, function( err ){
				mem.get(null, 'values', function( err, values ){
					assert.equal( values.length, 3);
					assert.equal( values[1], 3 );
					done()
				})
			});
		});
	});
});
