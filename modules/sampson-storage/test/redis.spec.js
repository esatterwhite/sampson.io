var assert = require('assert');
var Redis = require("../backends/redis");
var util = require('util')
describe('redis Store', function(){
	var redis = new Redis({name:"memory"});
	

	describe("#get / #set", function(){
		before( function( done ){
			redis.flush('rtest', done );
		});

		it('should set nested values', function(done){
			 redis.set( 'rtest', 'foo.bar.baz', 1,function(){
			 	redis.get('rtest','foo', function( err, data ){
			 		assert.equal( data.bar.baz, 1, "expected 1 got " + data.bar );
			 		done();
			 	});
			 });
		});


		it('should expire keys', function( done ){

			redis.set('rtest','key', 1, function(err){
				redis.ttl('rtest', 'key', 1, function(err){
					setTimeout(function(){
						redis.get('rtest','key', function( err, key){
							assert.equal( key, null);
							done();
						});
					},1500);
				});
			});
		});
	});

	describe('#push', function(){
		before( function( done ){
			redis.flush('rtest', done );
		});
		it('should convert values to arrays', function( done ){
			redis.set('rtest', 'test.rooms', 1, function( err1 ){
				redis.push('rtest', 'test.rooms', 2, function( err2){
					redis.get('rtest', 'test.rooms', function( err3, data ){
						assert.ok( Array.isArray( data ));
						assert.equal( data[0], 1 );
						assert.equal( data[1], 2 );
						done();
					});
				});
			});
		});


		it('should accept arrays of values', function( done ){
			redis.set('rtest', 'test.list', 1, function( err1 ){
				redis.push('rtest', 'test.list', [2,3,4], function( err2){
					redis.get('rtest', 'test.list', function( err3, data ){
						assert.ok( Array.isArray( data ), "not an array");
						assert.equal( data[0], 1, 'expected 1' );
						assert.equal( data[1], 2, 'exprected 2' );
						assert.equal( data[2], 3, 'expected 3' );
						assert.equal( data[3], 4, 'expected 4' );
						done();
					});
				});
			});
		});		
	});

	describe('#slice', function(){
		before(function( done ){
			redis.flush('rtest');
			redis.push('rtest','values.list',[1,2,3,4], done)
		})


		it('should remove items from the array', function(done){
			redis.slice('rtest', 'values.list', 2, function( err ){
				redis.get('rtest', 'values.list', function( err, values ){
					assert.equal( values.length, 3);
					assert.equal( values[1], 3 );
					done()
				})
			});
		});
	});
});
