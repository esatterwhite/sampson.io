var assert = require('assert');
var Mongo = require("../backends/mongo");
var util = require('util')

describe('Mongo Store', function(){
	var mongo;
	before( function(done ){
		mongo = new Mongo({
			name:"mtest"
		  , index:'pk'
		  , onConnect: done
		});
	})
	
	after(function( done ){
		mongo && mongo.flush( 'mtest', done )
	});

	describe("#get / #set", function(){
		before( function( done ){
			mongo.flush('mtest', done );
		});

		it('should set nested values', function(done){
			 mongo.set( 'mtest', 'foo.bar.baz', 1,function(){
			 	mongo.get('mtest','foo', function( err, data ){
			 		assert.equal( data.bar.baz, 1, "expected 1 got " + data.bar );
			 		done();
			 	});
			 });
		});


		it('should expire keys', function( done ){

			mongo.set('mtest','key', 1, function(err){
				mongo.ttl('mtest', 'key', 1, function(err){
					setTimeout(function(){
						mongo.get('mtest','key', function( err, key){
							assert.equal( key, null);
							done();
						});
					},1500);
				});
			});
		});
	});

	describe('#push', function(){
		before( function( done ){
			mongo.flush('mtest', done );
		});

		// it('should convert values to arrays', function( done ){
		// 	mongo.set('mtest', 'test.rooms', 1, function( err1 ){
		// 		mongo.push('mtest', 'test.rooms', 2, function( err2){
		// 			mongo.get('mtest', 'test.rooms', function( err3, data ){
		// 				assert.ok( Array.isArray( data ));
		// 				assert.equal( data[0], 1 );
		// 				assert.equal( data[1], 2 );
		// 				done();
		// 			});
		// 		});
		// 	});
		// });


		it('should accept arrays of values', function( done ){
			// todo: handle scalar values to Arrays
			mongo.push('mtest', 'test.list', [1,2,3,4], function( err2){
				mongo.get('mtest', 'test.list', function( err3, data ){
					assert.ok( Array.isArray( data ), "not an array");
					assert.equal( data[0], 1, 'expected 1' );
					assert.equal( data[1], 2, 'exprected 2' );
					assert.equal( data[2], 3, 'expected 3' );
					assert.equal( data[3], 4, 'expected 4' );
					done();
				});
			});
		});		
	});

	describe('#slice', function(){
		before(function( done ){
			mongo.flush('mtest');
			mongo.push('mtest','values.list',[1,2,3,4], done)
		})

		it('should remove items from the array', function(done){
			debugger;
			mongo.slice('mtest', 'values.list', 2, function( err ){
				mongo.get('mtest', 'values.list', function( err, values ){
					assert.equal( values.length, 3);
					assert.equal( values[1], 3 );
					done()
				})
			});
		});
	});
});
