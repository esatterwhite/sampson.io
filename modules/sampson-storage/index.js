/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'use strict';
/**
 * Base in memory storage for sampson
 * @module module:sampson-storage/backends/memory
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires prime
 * @requires module:lib/class/options
 * @requires options
 * @requires events
 * @requires sampson-core
 * @requires sampson-conf
 */

var Prime    = require( 'prime' )                      // Prime
  , Options  = require( '../../lib/class/options' ) // Options
  , events   = require( 'events' )                     // node events module
  , core     = require( 'sampson-core' )               // core
  , conf     = require( 'sampson-conf' )               // conf
  , backends = require('./backends')
  , Store
  ;


module.exports = Prime({

	inherits:events.EventEmitter
	,mixin:Options
	,options:{
		name:null
		,backend:{
			name:null
			,opts:{ }
		}
	}
	,constructor: function( options ){

		this.setOptions( options );
		this.configureBackend();
		this.setupEvents();
	}

	,configureBackend: function( ){
		if( this.options.backend.name ){
			backends.use( this.options.backend.name, this.options.backend.opts );
		}

		this.backend = backends.current;
	}
	,get: function get( store, key, cb ){
	}

	, set: function set( store, key, value ){
	}

	, slice: function slice( store, key, value ){}
	, push: function push( store, key, value ){}
    , tt: function ttl( store, key, value ){}
    , close: function close( ){}
	, remove: function remove( store, key, cb ){}
	, all: function all( store ){}	
});
module.exports.backends = backends; 
module.exports.Redis    = backends.Redis;
module.exports.Mongo    = backends.Mongo;
module.exports.File     = backends.File;
module.exports.Rethink  = backends.Rethink;
module.exports.Memory   = backends.Memory;
