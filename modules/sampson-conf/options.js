/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'use strict';
/**
 * Module used to define short hand flags for cli flags
 * compaitible class.
 * @module sampson-conf/options
 * @author Eric Satterwhite
 * @since 0.0.1
 */

module.exports = {
		'p':{
			alias:'port'
			,type:Number
			,description:'Port number of the wallace server to bind to'
		}
		,'b':{
			alias:'with-storage'
			,'description': 'The backend used for internal storage, can be `redis`, `memory`, `file` or `couch`'
			,type:String
		}
		,'e':{
			alias:'sitesenabled'
			,'description':'A path to a directory of application configurations to be loaded'
			,type:String
		}
		,'c':{
			alias:"config"
			,'description':"path to a config override json file"
			,type:String
		}
		,'l':{
			alias:'with-log'
			,type:String
			,description:"The type of log backend you would like to use"
		}

}
