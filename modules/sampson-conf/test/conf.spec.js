var assert = require('assert')
  , conf   = require('../')



describe('Conf loader', function(){
	describe('configuration heirarchy', function(){

		it('should read a conf file', function(){
			assert.equal( conf.get('test:fake'), 1 )
			assert.equal( conf.get('port'), 4400 )
		});

		it('should read env variables',function(){
			assert.equal( conf.get('STORAGE_TEST_A'), 'foo' )
			assert.equal( conf.get('STORAGE_TEST_B'), 'bar' )

		});

		it('should read use default values',function(){
			assert.equal( conf.get('redis:port'), 6379 )
			assert.equal( conf.get('redis:host'), 'localhost' )

		})
	});

	describe('application files',function( ){

		it('should accept an onLoad callback', function(done){
			conf.onLoad(function(){
				assert.ok( conf.get('applications:alpha'))
				done()
			})
		});

		it('should read all json files in a directory',function( done ){
			conf.onLoad( function(){
				var apps = conf.get('applications');

				assert.ok( apps.hasOwnProperty('alpha') )
				assert.ok( apps.hasOwnProperty('beta') )
				assert.ok( apps.hasOwnProperty('delta') )
				assert.ok(  !apps.hasOwnProperty('dummy')  )
				done()
			})
		})

		it('should accept an onLoad multiple times', function(done){
			conf.onLoad(function(){
				assert.equal( conf.get('applications:beta:postback:client:connect:path'), '/test/beta')
				assert.equal( conf.get('applications:beta:postback:client:connect:port'), '8100')
				assert.equal( conf.get('applications:beta:postback:client:connect:method'), 'post')
				done()
			})
		})
	});

	it('should allow values to be set',function( done ){
		conf.onLoad( function(){
			assert.doesNotThrow(function(){
				conf.set('foo:bar:baz', {key:'value'})
			});

			assert.equal( conf.get('foo:bar:baz:key'), 'value')
			done();
		})
	});
});
