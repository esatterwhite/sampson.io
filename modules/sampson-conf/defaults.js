'use strict';
/**
 * defaults.js
 * @module defaults.js
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires path
 */

var path = require('path');

exports.log = {
	syslog:{
		 host:"localhost"
		,port:undefined
		,app:process.title
		,protocol:'udp4'
		,type:"BSD"
		,facility:"local0"
	}

	,stdout:{
		label:"sampson " + process.pid
		, prettyPrint:true
		, colorize:true
		, exitOnError:false
		, timestamp:true
		, level:"emerg" 
	}

	,stderr:{

	}

	,file:{
		label:"sampson " + process.pid
	    , dir:"."
		, filename: path.join( process.cwd(), 'sampson.log' )
		, prettyPrint:false
		, level:"emerg"
		, json: false
		, options:{
			highWatermark:24
			,flags:'a'
		}
	}
};

exports.redis = {
	port:6379
  , host:'localhost'
  , opts:{}
};

exports.mongodb = {
	port: 27017
  , host:'localhost'
  , opts:{}
};

exports.storage = 'mongodb'

exports.browser = {
	allow:{
		head:true
	  , options: true
	}
};

exports.sitesenabled = path.resolve('.');
exports.port = 4000;
