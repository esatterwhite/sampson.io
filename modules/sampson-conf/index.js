/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Loads configuration form different data stores before the server starts
 * @module sampson-conf
 * @author Eric Satterwhite 
 * @since 0.0.1
 * @requires nconf
 * @requires path
 * @requires defaults
 */
var nconf    = require( 'nconf' )
	, defaults = require( './defaults' )
	, options  = require( './options' )
	, path     = require( 'path' )
	, os       = require( 'os' )
	, debug    = require( 'debug' )('modules:conf') 
	, walk     = require( 'walk' )
	, fs       = require( 'fs' ) 
	, tmp      = nconf.argv().env({separator:'__'})
	, noop     = new Function
	, backend
	, configfile
	, startup
	, startConf
	, walker
	, conf_db
	, opts
	, conf
	;

var CONF_LOADED = false;

function capitalize( str ){
	var s = String( str )
	return s.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }); 
}

require('nconf-redis');
require('nconf-mongodb');
configfile = path.normalize( path.resolve( tmp.get("conf") || 'config.json' ) );

startup = nconf
		.argv( options )
		.env({separator:'__'})
		.defaults( defaults )

startConf = startup.get();
var storageType = startConf['storage'] || 'memory'
backend = nconf[capitalize( storageType ) ]

if( !backend ){
	debug("no nconf backend %s found");
	let modulepath = nconf.get(storageType + ':module');
	debug("looking up module at %s", modulepath )
	try{
		backend = require( modulepath );
	} catch( e ){
		debug('unable to load storage backend');
		debug('exiting');
		process.exit( 101 );	
	}
}

debug( 'loading with %s storage backend', storageType )

conf = new nconf.Provider();
opts = startup.get[storageType] || undefined

if(opts){
	opts.db = 'nconf'
}

conf_db = new backend( opts )


startup.remove('env')
startup.remove('file')
startup.remove('defaults')
startup = null

conf
	.argv( options )
	.use( conf_db.type, conf_db )
	.env({separator:'__'})
	.file( configfile )
	.defaults( defaults )

conf.load(function(){
	var sites = conf.get('sitesenabled')
	debug('reading %s',sites)
	walker = walk.walk(conf.get('sitesenabled'),{
		filters: ['node_modules', ".git", "modules" ]
	})
	var existingapps = conf.get('applications') || {}
	var siteregex = /\.json$/

	walker.on('end', function(){
		debug('application files loaded')
		CONF_LOADED = true; 
	});

	walker.on('file', function(root, stats, next){
		if( siteregex.test( stats.name ) ){
			
			let name = stats.name.replace( siteregex, '' );
			
			if( existingapps.hasOwnProperty( name ) ){
				debug("application %s already defined. Skipping", name);
				return next();
			}

			debug( 'loading application file %s', path.join(root,stats.name)  );
			let app = {};
			app[ name ] = require( path.join( root,stats.name) ) 
			conf.merge('applications', app );
		}
		next();
	})
});

module.exports = conf;

module.exports.reload = function(cb){
	// some backends don't have a load method...
	// some do...
	conf_db.load && conf_db.load( cb || noop );
};

module.exports.list = function( key, cb ){
	return conf.get( key, cb );
};

module.exports.onLoad = function( cb ){
	if( !CONF_LOADED ){
		walker.on('end', cb )
	}
	else( cb() )
}
