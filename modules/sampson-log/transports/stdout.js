'use strict';
/**
 * stdout
 * @module stdout
 * @author Eric satterwhite
 * @since 0.0.1
 * @requires winston
 * @requires moduleB
 * @requires moduleC
 */

var winston = require( 'winston' )
  ;


module.exports = winston.transports.Console
