/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'use strict';
/**
 * file
 * @module file
 * @author Eric satterwhite
 * @since 0.0.1
 * @requires winston
 */

var winston = require( 'winston' );
module.exports = winston.transports.DailyRotateFile;
