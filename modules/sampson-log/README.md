sampson-log
===========

This package enables multi-transport logging built on top of [winston](https://www.npmjs.org/package/winston). By default sampson ships with three loggers, `stdout`, `file` and `syslog`. You specify which logger you wish to use by passing the `with-log` flag

#### Stdout

Simple output to stdout if available. Stdout is a blocking transport and should not be used in production set ups.

#### File

Uses winston's daily log file rotation transport to write to. Specify the `filename` option to dictate where the log files will be dumped.

```js
brock start --with-log=file --file:filename=/var/log/sampson.log
```

#### Syslog

The syslog module is specifically for posix systems and currently uses C bindings over syslog servers

```js
brock start --with-log=stdout
```

You can enable multiple logging transports by passing the `with-log` flag multipl times

```js
brock start --with-log=stdout --with-log=file
```

### Ad-Hoc Logging

Any node module that exports a winston compatible `Transport` class can be added to sampsons logger. To do so you use the `with-log` flag to name your logger type and can pass options via colon separated flags in the format of `name`:`option`. You *must* specify a module option which is an absolute path the the module to load. All other options will be passed the the constructor.

```js
brock start --with-log=sentry --sentry:module=/home/root/node_modules/sentry-logger --sentry:host=localhost --sentry:dsn=http://...
```
