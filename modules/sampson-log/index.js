/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'use strict';
/**
 * primary logging harness for sampson. Profides stdout, file and syslog logging by
 * default. Also allows for ad-hoc logging module loading given the module is a winston
 * compaitible class.
 * @module sampson-log
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires sampson-conf
 * @requires winston
 * @requires path
 * @requires util
 * @requires domain
 * @requires events
 * @requires module:mout/array/compact
 */

var winston      = require( 'winston' )          // winston logging module
  , conf         = require( 'sampson-conf' )     // configuration package for sampson
  , path         = require('path')               // node path module
  , util         = require('util')               // node util module
  , domain       = require( 'domain' )           // node domain module
  , events       = require( 'events' )           // node events module
  , compact      = require('mout/array/compact') // mout compact module 
  , loggerdomain = domain.create()               // domain object for logging
  , loggers      = []                            // container to hold logger objects loaded
  , log_types                                    // typs of loggers to enable, captured from config
  , exceptionloggers                             // logger to deal with errors specifically
  , loggerdomain                                 // Domain for logging to run under
  , logger                                       // the logger object to be exported
  , emitter                                      // error emitter for logging domin
  , log                                          // logging configuration
  , cli                                          // stdout logging object
  , log_dir                                      // directory to dump log files
  , stderr_log                                   // path for error logs
  , DEBUG                                        // Flag to enable stdout logging
  ;

log        = conf.get('log');
log_types  = conf.get('with-log');
log_types  = compact( Array.isArray( log_types ) ? log_types : [ log_types ] );
log_dir    = log.file.dir;
stderr_log = path.join(log_dir,'sampson.error.log');
emitter    = new events.EventEmitter();

// try to resolve a module to load a
// logging backend
log_types.forEach(function( type ){
	let backend = null;
	try{
		backend = require("./transports/" + type )
	} catch( err ){
		let backendconf = conf.get( type );
		if( backendconf && backendconf.module ){
			backend = require( backendconf.module );
		} else{
			let e = new Error();
			e.name="InvalidLogType";
			e.message = util.format( "unable to load logging module %s", type);
			emitter.emit('error', e);
		}
	}
	if( backend ){
		loggers.push( new backend( log[ type ] ) );
	}
})



exceptionloggers = [
		new winston.transports.DailyRotateFile({ 
			filename: stderr_log
		  , prettyPrint:true
		  , label:'errors' 
		})
];


loggerdomain.on('error', function( err ){
	process.stderr.write("problem writing to log %s\n %s", err.message, err.stack )
})


// run the loggers under a domain
loggerdomain.run( function(){
	logger = new (winston.Logger)({
		transports:loggers,
		exceptionHandlers: exceptionloggers,
		addColors:true,
		levels:winston.config.syslog.levels,
		colors:winston.config.syslog.colors,
		padLevels:true
	});
});


logger.exception = winston.exception
module.exports = logger;
