/*jshint laxcomma:true, smarttabs: true */
'use strict';
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires prime
 * @requires mout/object/merge
 * @requires mout/array/append
 **/
var Prime  = require('prime')
  , merge  = require( 'mout/object/merge' )
  , append = require( 'mout/array/append' )

function removeOn( name ){
	return name.replace(/^on([A-Z])/, function(full, first ){
		return first.toLowerCase();
	})
}


/**
 * Options and configuration mixin class
 * @class module:class/options
 * @param {Object} options Instance configuration options
 * @example var x = new NAME.Thing({});
 */
function Options( ){}


Object.defineProperties(Options.prototype,{
	/**
	 * clones and merges instance configuration and stores it on the `options` instance property.
	 * if an option has the name onFoo, and the instance uses events, the `foo` event handler will be added
	 * @param {TYPE} name DESCRPTION
	 * @param {TYPE} name DESCRIPTION
	 * @returns {TYPE} DESCRIPTION
	 */ 
	setOptions: {
		value:function( options ){
			this.options = merge.apply(null, append([{}, this.options], arguments ) );
			options = this.options;
			if( !!this.addListener ){
				for( var opt in options ){

					if( typeof( options[ opt ] ) !== 'function' || !(/^on[A-z]/).test(opt)){
						continue;
					}
					this.addListener( removeOn( opt ), options[ opt ]);
					delete options[opt];
				}
			}
			return this;
		}
		,enumerable:true
		,writeable: true
		,configurable:true
	}
})

module.exports = Options
