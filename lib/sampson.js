#!/usr/bin/env node
/*jshint laxcomma: true, smarttabs: true*/
/*globals require,process,__dirname */
/*globals console*/
'use strict';

var   util    = require('util')
	, words   = require('../include/names' )
	, log     = require("sampson-log")
	, api     = require('sampson-api')
	, conf    = require('sampson-conf')
	, http    = require("http")
	, express = require('express')
	, local_server = false
	;

function random( arr ){
	let rand = Math.floor( Math.random() * ( arr.length - 1 )  );
	return ( arr.length ) ? arr[ rand ] : null;
};
process.title = util.format( 'sampson %s', random( words ).replace(/'s/, '') );

exports.start = function( app, option ){
	if( !app ){
		app = express();
		local_server = true;
		app.listen( conf.get('port'));
	}

	for( let key in api ){
		app.use( util.format("/%s", key), api[key] )
	}

	log.info( 'server running on port %s', conf.get('port'))
}

exports.stop  = function(){

}

