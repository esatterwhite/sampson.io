#!/usr/bin/env node
/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'use strict';
/**
 * sampson test runner harness
 * @module runner
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires child_process
 * @requires module:mout/lang/clone
 * @requires fs
 * @requires path
 * @requires os
 * @requires util
 */

var child_process = require('child_process')               // child proces for spawning mocha
  , clone         = require('mout/lang/clone')             // object clone module
  , fs            = require('fs')                          // fs module
  , path          = require('path')                        // path module
  , os            = require('os')                          // os module
  , util          = require("util")                        // util module
  , production    = (process.env.NODE_ENV == 'production') // boolean flag if we are in producion mode
  , env           = clone( process.env )                   // clone of current process env
  , debug         = require('debug')( 'scripts:runner')
  , npath         = ( env.NODE_PATH || "" ).split(':')     // cache of node path split into an array
  , html                                                   // html stream
  , coverage                                               // mocha code coverage process
  , mocha                                                  // moacha child process
  , reporter


// add our modules director to node require path
// so we don't have to ../../../../../
npath.push(path.resolve(__dirname,'..','modules') )
npath = npath.join( os.platform == 'win32' ? ';' : ':')

env.NODE_PATH = npath  

// inject some test vars into the env
// for the conf loader to read / test

env.STORAGE_TEST_A = "foo";
env.STORAGE_TEST_B = "bar";
env.storage='memory';

env.conf = path.resolve( __dirname, '..' ,'modules','sampson-conf', 'test','conf','test.json' )
env.sitesenabled = path.resolve( __dirname, '..' ,'modules','sampson-conf', 'test', 'applications' )

if( production ){
	reporter = fs.createWriteStream('tap.xml',{
		flags:'w'
		,encoding:'utf8'
	})
} else {
	html = fs.createWriteStream('coverage.html',{
		flags:"w"
		,encoding:'utf8'
	});
	coverage = child_process.spawn("mocha", ["--harmony", "--recursive", "-r", "jscoverage", "--reporter=html-cov",  "test", "modules"],{
		env:env		
	})
	coverage.stdout.pipe( html );
	reporter = process.stdout
}

mocha = child_process.spawn("mocha", [
	"--harmony"
  , "--growl"
  , "--recursive"
  // ,"--debug-brk"
  , util.format("--reporter=%s", production ? 'xunit':'spec')
  , "test"
  , "modules"
], { env:env });

mocha.stdout.pipe( reporter )
mocha.stderr.pipe( reporter )
