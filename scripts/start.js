#!/usr/bin/env node
'use strict';

var child_process = require("child_process")   //
  , spawn         = child_process.spawn        //
  , path          = require('path')            //
  , clone         = require('mout/lang/clone') //
  , os            = require('os')              //
  , env           = clone( process.env )       //
  , args          = clone( process.argv )      //
  , npath         = ( env.NODE_PATH || '').split(':')   //
  ;

args.splice(0,2)
args.unshift('server.js')
args.unshift('--harmony')

npath.push(path.resolve(__dirname,'..','modules') )
npath = npath.join( os.platform == 'win32' ? ';' : ':')

env.NODE_PATH = npath  

var proc = spawn('node', args, {
	cwd:path.resolve(__dirname, '..' )
	,env:env
}, function(err, stdout, stderr ){

	console.log( "error", err )
	console.log( stdout, stderr  )
})
proc.stdout.pipe( process.stdout )
proc.stderr.pipe( process.stderr )
