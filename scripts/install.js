#!/usr/bin/env node
/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'strict mode'
/**
 * installs the dependacies of the packages found in moudles
 * @module module:scripts/install
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires sampson-core
 * @requires debug
 * @requires child_process
 * @requires fs
 * @requires path
 * @requires util
 * @requires mout/lang/clone
 */
var debug         = require('debug')('scripts:install')
  , child_process = require('child_process')
  , fs            = require('fs')
  , path          = require('path')
  , util          = require('util')
  , core          = require('../modules/sampson-core')
  , spawn         = child_process.spawn
  ;


// generates a list of semver packages names ( foo@x.x.x ) for npm to install
function resolveDeps( dependancies ){
	dependancies = dependancies || {};
	var ret = []
	for( var dep in dependancies ){
		ret.push( util.format("%s@%s", dep, dependancies[dep] ) );
	}
	return ret
};


// TODO: should handle optional deps as optional
fs.readdirSync( core.paths.modules ).forEach( function( module ){
	var packagefile = path.join( core.paths.modules, module, "package.json" )

	debug("reading %s",  module )
	if( fs.existsSync( packagefile ) ){
		var deps = resolveDeps( require(packagefile).dependencies )
		debug( 'installing', deps )
		var proc = spawn('npm', ['install','--prefix', core.paths.root].concat(deps), {
			cwd:path.join( core.paths.root )
			,env: process.env
		}, function(err, stdout, stderr ){

			console.log( "error", err );
			console.log( stdout, stderr  );
		})
		proc.stdout.pipe( process.stdout );
		proc.stderr.pipe( process.stderr );
	}
})